#include <ros/ros.h>

#include "r2_translator/r2_translator.h"
#include "r2_translation_msgs/R2Trajectory.h"
#include <nasa_r2_common_msgs/LabeledJointTrajectory.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>

static const char* const R2_JOINT_TRAJECTORY_TOPIC  = "/joint_refs";

void translateTrajectory(const r2_translation_msgs::R2TrajectoryConstPtr& msg, ros::Publisher* pub)
{
    if (pub->getNumSubscribers() < 1)
    {
        ROS_WARN("There are no subscribers to %s.  NOT translating trajectory", R2_JOINT_TRAJECTORY_TOPIC);
        return;
    }

    // Translating the trajectory message into the specific R2 message for the publisher
    nasa_r2_common_msgs::LabeledJointTrajectory labeled_trajectory_msg;
    labeled_trajectory_msg.header.stamp = ros::Time::now();
    labeled_trajectory_msg.header.frame_id = msg->frame_id;

    labeled_trajectory_msg.originator = msg->originator;
    labeled_trajectory_msg.joint_names = msg->joint_names;

    for (size_t i = 0; i < msg->points.size(); ++i)
    {
        trajectory_msgs::JointTrajectoryPoint point;
        point.positions = msg->points[i].positions;
        point.velocities = msg->points[i].velocities;
        point.accelerations = msg->points[i].accelerations;
        point.time_from_start = msg->points[i].time_from_start;
        labeled_trajectory_msg.points.push_back(point);
    }

    //ROS_INFO("Publishing trajectory message");
    pub->publish(labeled_trajectory_msg);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "R2Translator");
    ros::NodeHandle n;

    ros::Publisher trajectoryPub = n.advertise<nasa_r2_common_msgs::LabeledJointTrajectory>(R2_JOINT_TRAJECTORY_TOPIC, 1);
    ros::Subscriber trajectorySub = n.subscribe<r2_translation_msgs::R2Trajectory>(R2_TRAJECTORY_TRANSLATION_TOPIC, 10, boost::bind(&translateTrajectory, _1, &trajectoryPub));

    // Spin for-ev-er. FOR-ev-er.  FOR-EV-ER.
    ros::spin();
}
